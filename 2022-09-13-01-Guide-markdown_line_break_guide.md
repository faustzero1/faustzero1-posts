<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# Markdown Line Break Guide

Date published: 2022-09-13

---

## No Line Break

```
Hello(<-- No Spaces)
World
```  
Result:  
Hello
World

## Line Break (using Two Spaces)

```
Hello  (<-- Two Spaces)
World
```  

Result:  
Hello  
World  

---

## [OPTIONAL] Line Break (using br tags)

```
Hello <br>
World
```

Result:  
Hello <br>
World


## [NOT] a Line Break

```
Hello

World
```

Result:  
Hello

World
