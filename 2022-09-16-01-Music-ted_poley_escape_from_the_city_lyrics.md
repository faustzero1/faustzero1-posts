<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# Ted Poley - Escape From the City (from Sonic Adventure 2) Lyrics

Date published: 2022-09-16

---

## Escape From the City Lyrics

```
[Intro]  
Whoo!
Oh yeah!

[Verse 1]  
Rollin' around at the speed of sound 
Got places to go, gotta follow my rainbow
Can't stick aound, have to keep movin' on
Guess what lies ahead, only one way to find out!

[Post-Chorus 1]  
Must keep on movin' ahead
No time for guessin', follow my plan instead
Trustin' in what you can't see
Take my lead, I'll set you free

[Chorus]
Follow, set me free
Trust me and we will escape from the city
I'll make it through, follow me

Follow me, set me free
Trust me and we will escape from the city
I'll make it through; prove it to you

Follow me!
Oh yeah!

[Verse 2]  
Danger is lurking around every turn
Trust your feelings, got to Live and Learn
I know with some luck that I'll make it through
Got no more other options, only one thing to do!

[Post-Chorus 1]  
I don't care what lies ahead
No time for guessin', follow my plan instead
Find that next stage, no matter what that may be
Take my lead, I'LL set you free

[Chorus]
Follow, set me free
Trust me and we will escape from the city
I'll make it through, follow me

Follow me, set me free
Trust me and we will escape from the city
I'll make it through; prove it to you

Follow me!
Follow me!

[Outro]
I'll make it through
Oh yeah!
```
