<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# Breaking Benjamin - Red Cold River Lyrics

Date published: 2022-09-23

---

## Red Cold River Lyrics

```
[Verse 1]
Days reborn
Fight with folded hands
Pain left below
The lifeless live again

[Pre-Chorus]
Run, run, run
Red cold river
Run, run, run
Red cold river

[Chorus]
I can't feel anything at all
This life has left me cold and damned
I can't feel anything at all
This love has led me to the end

[Verse 2]
Stay reformed
Erase this perfect world
Hate left below
The dark stray dof of war

[Pre-Chorus]
Run, run, run
Red cold river
Run, run, run
Red cold river

[Chorus]
I can't feel anything at all
This life has left me cold and damned
I can't feel anything at all
This love has led me to the end

[Bridge]
Try to find a reason to live
Try to find a reason to live
Try to find a reason to live
No!

[Chorus]
I can't feel anything at all
This life has left me cold and damned
I can't feel anything at all
This love has led me to the end
```
