<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# Keith Power - We All Lift Together (from Warframe) Lyrics

Date published: 2022-09-13

---

## We All Lift Together Lyrics

```
[Intro: All]  
*Whistling, then humming*

[Chorus: Kevin Durand]  
Cold, the air and water flowing
Hard, the land we call our home
Push, to keep the dark from coming
Feel the weight of what we owe

[Post-Chorus: Kevin Durand, Murray Foster & Jason Lewis]  
This, the song of sons and daughters
Hide, the heart of who we are
Making peace to build our future
Strong, united, working 'till we fall

[Chorus: Kevin  Durand & Dammhnait Doyle]
Cold, the air and water flowing
Hard, the land we call our home
Push, to keep the dark from coming
Feel the weight of what we owe

[Post-Chorus: Kevin Durand, Jason Lewis & Damhnait Doyle]
This, the song of sons and daughters
Hide, the heart of who we are
Making peace to build the future
Strong, united, working 'till we fall

[Outro: All]
And we all lift...
And we're all adrift...Together...Together...
Through the cold mist... 
'Till we're lifeless...Together...Together...
```
