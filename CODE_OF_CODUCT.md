# faustzero1-posts Code of Coduct:

## Markdown file naming:

There is 3 naming section for each files, Each section will divided by dash ```-```  
* 1st Section will contain **Publication status** of the post.  
if the post are already published it is must use ```YYYY-MM-DD-NN``` aka **ISO** format (with additional *Natural Number*), mean Year-Month-Day-NN, while NN mean the NaturalNumber of the post list in order of the day.  
if the post is the **1st post** of the day, so the 1st section must be ```YYYY-MM-DD-01```  
if the post is the **2nd post** of the day, so the 1st section must be ```YYYY-MM-DD-02```, and so on.  
if the post are unpublished so the date will be replaced with **WIP** means *Work in Progress*.  
* 2nd Section will contain **Category name** of the post.  
Must use Upper case for the first letter of the word name.  
* 3rd Section will contain **Title name** of the post.  
Must use lower case.  
Each space must use underscore ```_```  


Example Layout:  
```
<YYYY-MM-DD>_dash_<Category>_dash_<post_title>_dot_md
```
Another Example Layout if status =  Work in Progress:  
```  
WIP_dash_<Category>_dash_<post_title>_dot_md
```


Example Usage:  
```
2022-09-23-Guide-void_linux_installation_guide_glibc_edition_for_x86_64.md
```  
Another Example Usage if status = Work in Progress:  
```
WIP-Guide-void_linux_installation_guide_glibc_edition_for_x86_64.md
```

### Why?

So the user can: 
* Easily filter if the post already **Published or still Work in Progress** by seeing the **1st Section**.  
eg: if there is a **DATE**, like ```2022-09-23-Etc-etc``` so the post are already *Published*.  
eg: if there is no date but a word **WIP**, like ```WIP-Etc-etc``` so the post are still *Work in Progress*.  
* Easily filter and sort by ```Date-Published``` by seeing **1st Section** of the file name.  
eg: ```ls | grep 2022-09``` to filter *any post that pusblished in 2022-09*.
* Easily filter and sort  by ```Category``` by seeing **2rd Section** of the file name.  
eg: ```ls | grep Guide``` to filter *any post with Guide as it Category*.
<!-- * Easily sort by ```Date-modified``` by using ??? -->

<!--
## Markdown header:
-------------------

+++
Title: "Void Linux Installation Guide glibc edition for x86_64"
Summary: "A Notes about Installing Void Linux glibc edition for x86_64."

Date: 2022-09-23
Category: "Note"
Tags:["Tips"; "Linux"; "Guide"]
License: "CC-BY-NC-SA-4.0"

wip: true
+++
-->
