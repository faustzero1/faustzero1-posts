<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# STARSET - Carnivore Lyrics

Date published: 2022-09-23

---

## STARSET - Carnivore Lyrics

```
[Verse 1]
All my life, they let me know
How far I would not go
But inside the beast still grows
Waiting, chewing through the ropes

[Pre-Chorus]
Who are you to change this world?
Silly boy
No one needs to hear your words
Let it go

[Chorus]
Carnivore, carnivore
Won't you come digest me?
Take away everything I am
Bring it to an end
Carnivore, carnivore
Could you come and change me?
Take away everything I am
Everything I am

[Verse 2]
I will hide myself below
I'll be what you wanted
Kept inside, I won't let go
'Till I burn beyond control

[Pre-Chorus]
Who are you to change this world?
Silly boy
No one needs to hear your words
Let it go

[Chorus]
Carnivore, carnivore
Won't you come digest me?
Take away everything I am
Bring it to an end
Make me fall, make me bleed
Go ahead and change me
Take away everything I am
Everything I am

[Bridge]
Never enough
(Who I am is not good enough)
Never enough
(Who I am)

[Chorus]
Carnivore, carnivore
Won't you come digest me?
Take away everything I am
Bring it to an end
Carnivore, carnivore
Could you come and change me?
Take away everything I am
Everything I am
```
