<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# Breaking Benjamin - Dear Agony Lyrics

Date published: 2022-09-23

---

## Dear Agony Lyrics

```
[Verse 1]
I have nothing left to give
I have found the perfect end
You were made to make it hurt
Disappear into the dirt
Carry me to heaven's arms
Light the way and let me go
Take the time to take my breath
I will end where I began

[Pre-Chorus]
And I will find the enemy within
Because I can feel it crawl beneath my skin

[Chorus]
Dear Agony
Just let go of me
Suffer slowly
Is this the way it's got to be?
Dear Agony

[Verse 2]
Suddenly
The lights go out
Let forever
Drag me down
I will fight for one last breath
I will fight until the end

[Pre-Chorus]
And I will find the enemy within
Because I can feel it crawl beneath my skin

[Chorus]
Dear Agony
Just let go of me
Suffer slowly
Is this the way it's got to be?
Don't bury me
Faceless enemy
I'm so sorry
Is this the way it's got to be?
Dear Agony

[Bridge]
Leave me alone
God, let me go
I'm blue and cold
Black sky will burn
Love, pull me down
Hate, lift me up
Just turn around
There's nothing left

[Pre-Chorus]
Somewhere far beyond this world
I feel nothing anymore

[Chorus]
Dear Agony
Just let go of me
Suffer slowly
Is this the way it's got to be?
Don't bury me
Faceless enemy
I'm so sorry
Is this the way it's got to be?
Dear Agony

[Outro]
I feel nothing anymore
```
