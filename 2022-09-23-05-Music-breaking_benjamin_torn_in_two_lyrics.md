<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# Breaking Benjamin - Torn in Two Lyrics

Date published: 2022-09-23

---

## Torn in Two Lyrics

```
[Verse 1]
Is this the way it's got to be?
Ignite the fire inside of me
Embrace the life of tragedy
A tide of war and broken dreams

[Chorus]
I am torn in two
Hold on, hold on
We're barely alive
I am faded through
Hold on, hold on
The fallen arise
I will fight this war for you
And let the dawn of love survive
Broken, I crawl back to life

[Verse 2]
Is this the fate we fall between?
Deface the life inside of me
Drain the heart of atrophy
And take away the remedy

[Chorus]
I am torn in two
Hold on, hold on
We're barely alive
I am faded through
Hold on, hold on
The fallen arise
I will fight this war for you
And let the dawn of love survive
Broken, I crawl back to life

[Chorus]
I am torn in two (Please hold on)
Hold on, hold on
We're barely alive
I am faded through (Please hold on)
Hold on, hold on
The fallen arise
I will fight this war for you (Please hold on)
And let the dawn of love survive
Broken, I crawl back to life

[Outro]
Hold on
Rise
Hold on
Rise
Hold on
Rise
Hold on
```
