<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# STARSET - It Has Begun Lyrics

Date published: 2022-09-17

---

## It Has Begun Lyrics

```
[Verse 1]
Even a well lit place, can hide salvation
A map to a one man maze that never sees the sun
Where the lost are the heroes
And the thieves are left to drown
But everyone knows by now
Fairy-tales are not found
They are written in the walls

[Chorus]
As we walk in a straight line
Down in the dirt with a landslide approaching
But nothing could ever stop us
From stealing our own place in the sun
We will face the odds against us
And run into the fear we run from (ah, ah)
It has begun


[Verse 2]
Into the dark below
Evading shadows
Blind in a rabbit's hole
We fall beneath the earth
And watch the shell come unraveled
As the seed begins to rise
Embracing a starlit fate as we wait in the night
It's written in the walls

[Chorus]
As we walk in a straight line
Down in the dirt with a landslide approaching
But nothing could ever stop us
From stealing our own place in the sun
We will face the odds against us
And run into the fear we run from (ah, ah)

[Outro]
It has begun (ah, ah)
It has begun (ah, ah)
It has begun (ah, ah)
It has begun (ah, ah)
It has begun
```
