<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# Mick Gordon (feat. Tex Perkins) - The Partisan (from Wolfenstein: The Old Blood) Lyrics

Date published: 2022-09-16

---

## The Partisan Lyrics

```
When they poured across the border
I was cautioned to surrender
This I could not do;
I took my gun and vanished

I have changed my name so often
I've lost my wife and children
But I have many friends
And some of them are with me

An old woman gave us shelter
Kept us hidden in the garret
Then the soldier came;
She died without whisper

There were three of us this morning
I'm the only one this evening
But I must go on;
The frontiers are my prison

Oh, the wind, the wind is blowing~
Through the graves the wind is blowing~
Freedom soon will come;
Then we'll come from the shadow





Oh, the wind, the wind is blowing...
Through the graves the wind is blowing...
Freedom soon will come;
Then we'll come from the shadows


From the Shadows!
From the Shadows!
Ohh!
From the Shadows!
From the Shadows!
Shadows!
From the Shadows!

```
