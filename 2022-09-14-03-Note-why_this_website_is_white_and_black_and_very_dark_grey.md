<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# Why this website is White and Black and Very Dark Grey

Date published: 2022-09-14

---

## The only color use on this website  

* #FFF - aka White  
* #000 - aka Black  
* #111 - aka Very Dark Grey  

## Why

### UX purpose

* Black and White are contrast, the color that just work.  
* Easy to use, just flip the color to make it contrast.  
* Accessibility, I know there's so many people out there who use display monitor that doesn't have (near) perfect color accuracy, however black and white contrast can still just work on those.  

### My personal color livery

* F - Faust
* 0 - Zero
* 1 - One
