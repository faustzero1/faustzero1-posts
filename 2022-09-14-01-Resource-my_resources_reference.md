<nav>
<a href="../index.html">Home</a>
|
<a href="../post.html">Post</a>
|
<a href="../project.html">Project</a>
<nav class="div-right">
<a href="../contact.html">Contact</a>
|
<a href="../about.html">About</a>
</nav>
</nav>
</header>
<hr><hr>
<main>
<!-- Your Content Start After This Line -->


# My Resources Reference

Date published: 2022-09-14

---

## Best  
> [suckless.org](https://suckless.org) - Software that suckless.  
> [cat-v](http://harmful.cat-v.org/software/) - *Considered harmful*.  
> [4chan /g/ Technology boards](https://boards.4channel.org/g/) - Least *anti-norms* social media.  

## Others  
> [Urban Dictionary's](https://www.urbandictionary.com/) - A place to find meanings about slang words and such.  
> [Teddit](https://teddit.net/) - An free open source front-end to **Reddit** focused on privacy that **doesn't require you to have JavaScript enabled** in your browser.  
 > [/r/unixporn](https://teddit.net/r/unixporn) - Subreddit about ricing (modding the looks and feels on Linux/BSD/*nix based OS).  
 > [/r/thinkpad](https://teddit.net/r/thinkpad) - Subreddit about ThinkPad (A glorious place to see ThinkPad things btw).  


> [Invidious](https://invidious.sethforprivacy.com/) - An open source alternative front-end to **YouTube**  
> [Odysee](https://odysee.com/) - Youtube alternative using LBRY as back-end, sometime I found the *Rabbit Hole* video's here.  
> [Pleroma | Fediverse](https://stereophonic.space/) - A fediverse social media that is lightweight.  

> [Tevvez](https://www.youtube.com/c/esteevteev) - Chad, Gen ZYZZ, Create awesome hardstyle music.  

## Youtube Channel of Cool based person on the internet:

> [Luke Smith](https://www.youtube.com/c/LukeSmithxyz/) - <s>Runescape Default Character</s> Based tech boomer that say "ok" so many times, use ThinkPad X200, T420, X60, X220 (mainly) with dwm, creator of [LARBS](https://github.com/LukeSmithxyz/LARBS), linguistic sometime, "what's up guys, vim diesel here".  
> [Mental Outlaw](https://www.youtube.com/c/MentalOutlaw/) - <s>Luke Smith Deepfake</s> <s>aka Jayson Tatum</s> One of few least tech news youtuber that you can trust, Gentoo user, anti-christ hater.  
> [Distro Tube](https://www.youtube.com/c/DistroTube) - Based boomer too. do many Linux / Free Software guide.  
> [Root BSD](https://www.youtube.com/channel/UCbpe1SYzZKG4RswEnxxSRZQ) - Based boomer, OpenBSD user.  
> [ThePrimeAgen](https://www.youtube.com/c/ThePrimeagen) - Die-hard vim user.  
